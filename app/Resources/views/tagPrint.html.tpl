{for $index=1 to $count}
<table align="center">
        <tbody align="center">
        <tr>
            <td>{$vendorName}</td>
          </tr>
          <tr>
            <td><span style="font-weight:bold;font-size: 25px;">{$serialNumber}</span></td>
          </tr>
          <tr>
            <td><span style="font-weight:bold;font-size: 25px;">{$index} of {$count}</span></td>
          </tr>
          <tr>
            <td class="special" >{if is_numeric($ugCount) && $index>$count-$ugCount}UG{else}{$type}{/if}</td>
          </tr>
          <tr>
            <td>{$customerName}</td>
          </tr>
    </tbody>
</table>
{/for}
</table>
<style type="text/css">
	table { 
	border-width: 1px;
	border:solid black;
	 page-break-inside:auto ;
}

tr    { page-break-inside:avoid; page-break-after:auto }

tbody {
    display:block;
    height:144px;
    width: 144px;
    overflow:auto;
}

 thead { display:table-header-group }
    tfoot { display:table-footer-group }
    
    td.special { border: 3px double black; }
</style>