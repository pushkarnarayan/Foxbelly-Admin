<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
<div class="container">
{if $success == 1}
<div id="successDiv" class="alert alert-success alert-dismissable">
 <a class="close" onclick="$('.alert').hide()">×</a> 
  <strong>Success!</strong> Order Updated.
</div>
{/if}
	<div class="row">
		<form class="form-horizontal"  method="post" id="datePickerForm" action="{'manual_entry_save'|path}">
<fieldset>

<legend>Manual Entry</legend>

<div class="form-group">
  <label class="col-md-4 control-label" for="Current Fleet Size">Serial Number</label>  
  <div class="col-md-5">
  <input id="serialNumber" name="serialNumber" type="text" placeholder="Serial Number" class="form-control input-md">
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button type="button" id="singlebutton" name="singlebutton" onclick="getSerialNumber();" class="btn btn-primary">Get</button>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Annual Forklift Purchases">Name</label>  
  <div class="col-md-5">
  <input id="name" name="name" type="text" placeholder="Customer name" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Annual Forklift Purchases">Phone Number</label>  
  <div class="col-md-5">
  <input id="contact" name="contact" type="text" placeholder="Contact Number" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Annual Forklift Purchases">Address</label>  
  <div class="col-md-5">
  <input id="address" name="address" type="textarea" placeholder="Address" class="form-control input-md">
  </div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="Annual Forklift Purchases">Pick Up Date</label>  
  <div class="col-md-5">
            <input type="text" id="datepicker" name="pickUpDate"  class="form-control" placeholder="Choose">
  </div>
 </div>
<div class="form-group">

  <label class="col-md-4 control-label" for="Decision Timeframe">Pick Up Time</label>
  <div class="col-md-5">
    <select id="pickUpTime" name="pickUpTime" class="form-control">
      <option value="10">10 -12 AM</option>
      <option value="12">12 - 2 PM</option>
      <option value="14">2 - 4 PM</option>
      <option value="16">4 - 6 PM</option>
      <option value="18">6 - 8 PM</option>
      <option value="20">8 - 10 PM</option>
    </select>
  </div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="Annual Forklift Purchases">Delievery Date</label>  
  <div class="col-md-5">
            <input type="text" id="datepicker_1" name="delieveryDate"  class="form-control" placeholder="Choose">
  </div>
 </div>
<div class="form-group">
  <label class="col-md-4 control-label" for="Decision Timeframe">Delievery Time</label>
  <div class="col-md-5">
    <select id="delieveryTime" name="delieveryTime" class="form-control">
      <option value="10">10 -12 AM</option>
      <option value="12">12 - 2 PM</option>
      <option value="14">2 - 4 PM</option>
      <option value="16">4 - 6 PM</option>
      <option value="18">6 - 8 PM</option>
      <option value="20">8 - 10 PM</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
  </div>
</div>

</fieldset>
</form>

	</div>
</div>

<script type="text/javascript">

$(function() {
        $( "#datepicker" ).datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            minDate: '0d'
        });
    });
   $(function() {
        $( "#datepicker_1" ).datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            minDate: '0d'
        });
    });

function getSerialNumber(){
  $.ajax({
    type: "POST",
    url: "{'get_serial_number'|path}",
    success: function(data) {
        console.log(data);
        $('#serialNumber').val(data);
    }
  });
}
  
</script>