<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- <div class="loader"></div> -->
<div>
<div id="successDiv" class="alert alert-success alert-dismissable" style="display: none;">
 <a class="close" onclick="$('.alert').hide()">×</a> 
  <strong>Success!</strong> Contact Updated.
</div>
<div id="failureDiv" class="alert alert-danger alert-dismissable" style="display: none;">
 <a class="close" onclick="$('.alert').hide()">×</a> 
  <strong>Failure!</strong>Contact not updated.
</div>
    <div class="row">
<!-- {$totalOrdersData|var_dump} -->
        <section class="content">
            <!-- <h1>Foxbelly Orders</h1> -->
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">
                            <div class="btn-group">
                                <button type="button" id="scheduled" class="btn btn-default btn-filter" data-target="scheduled">Scheduled</button>
                                <button type="button" id="pickup" class="btn btn-default btn-filter" data-target="pickup">Ready For Pick-Up</button>
                                <button type="button" id="inprocess" class="btn btn-default btn-filter" data-target="inprocess">In Process</button>
                                <button type="button" id="outfordelievery" class="btn btn-default btn-filter" data-target="outfordelievery">Out for Delievery</button>
                                <button type="button" id="delievered" class="btn btn-default btn-filter" data-target="delievered">Delievered</button>
                                <button type="button" id="cancelled" class="btn btn-default btn-filter" data-target="cancelled">Cancelled</button>
                            </div>

                        </div>
<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 25%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                            </div>
                        
                        <div class="table-container">
                            <!-- <table class="table table-filter">
                                <tbody>
                                 {foreach $totalOrdersData as $data}
                                   <tr data-status="scheduled">
                                  
                                        <td>
                                            <div class="ckbox">
                                                <input type="checkbox" id="checkbox2">
                                                <label for="checkbox2"></label>
                                            </div>
                                        </td>
                                        <td>
                                        {if $data['ordersActivity']['status'] != 50 &&$data['ordersActivity']['status'] != 60}
                                            <button type="button" {if $data['ordersActivity']['status'] == 30 && !isset($data['ordersActivity']['bill'])}disabled {/if}onclick="openMoveStageLightBox('{$data['ordersActivity']['ordersActivityId']}','{$data['ordersActivity']['status']}')" class="btn btn-default btn-lg">
                                        
  <span class="glyphicon glyphicon-chevron-right"aria-hidden="true"></span><a href="#myModal" role="button" class="btn" data-toggle="modal">

        Move to {$statusDisplayText[$data['ordersActivity']['status']+10]}
    </a>
</button>
{/if}
                                        </td>
                                        <td>
                                            <div class="media">
                                                <a class="pull-left">
                                                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                                </a>
                                                <div class="media-body">
                                                    <span class="media-meta pull-right">{($data['ordersActivity']['updatedOn']/1000)|date_format} </span>
                                                   
                                                    <h4 class="title">
                                                        {$data['order']['displayId']}
                                                        <span class="pull-right pagado">{$statusDisplayText[$data['ordersActivity']['status']]}</span>
                                                    </h4>
                                                    <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-user">
                        </i>{$data['order']['name']}</cite></small>
                         <p>
                        <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-earphone">
                        </i>{$data['order']['contact']}</cite></small>
                        </p>
                        <p>
                        <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-map-marker">
                        </i>{$data['order']['location']}</cite></small>
                        {if isset($data['order']['latLng'])}
                        <a target="_blank" href={"http://maps.google.com/?q="|cat:$data['order']['latLng']['lat']|cat:","|cat:$data['order']['latLng']['lng']}>Go to Map</a>
                        </p>
                        {/if}
                        <p>
                        <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-time">
                        </i>Pick Up-<b>{($data['order']['pickUpDate']/1000)|date_format} @ {$slotsList[$data['order']['pickUpTime']]['value']}</b></cite></small>
                        </p>
                        <p>
                        <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-time">
                        </i>Delievery-<b>{($data['order']['delieveryDate']/1000)|date_format} @ {$slotsList[$data['order']['delieveryTime']]['value']}</b></cite></small>
                        </p>
                        {if isset($data['ordersActivity']['contact'])}
                        <p>
                        <small><cite title="San Francisco, USA"><i class="glyphicon glyphicon-time">
                        </i>Foxbelly Contact: <b>{$data['ordersActivity']['contact']['name']} - {$data['ordersActivity']['contact']['number']}</b></cite></small>
                        </p>
                        {/if}
                        {if $data['ordersActivity']['status'] >= 30 && !isset($data['ordersActivity']['bill'])}
                        <a target="_blank" href="{'create_bill'|path}?ordersActivityId={$data['ordersActivity']['ordersActivityId']}">Create Bill</a>
                        {elseif $data['ordersActivity']['status'] >= 30 && isset($data['ordersActivity']['bill'])}
                        <a target="_blank" href="{'view_bill'|path}?ordersActivityId={$data['ordersActivity']['ordersActivityId']}&displayId={$data['order']['displayId']}">View Bill</a> (Total : Rs
                        {$data['ordersActivity']['bill']['totalBill']} /-)
                        
                        {/if}
                              

                                         </div>
                                            </div>
                                        </td>

                                    </tr>
                                    {/foreach}
                                </tbody>
                            </table> -->
                            <table>
  <tr>
    <th>Order Id</th>
    <th>Customer Detail</th>
    <th>Location</th>
    <th>Pick Up</th>
    <th>Delievery</th>
    <th>Foxbelly C</th>
    <th>Bill</th>
  </tr>
  {foreach $totalOrdersData as $data}
    <tr>
    <td>{$data['order']['displayId']}</td>
    <td>{$data['order']['name']}<br><b>{$data['order']['contact']}</b></td>
    <td>{$data['order']['location']} {if isset($data['order']['latLng'])}
                        <a target="_blank" href={"http://maps.google.com/?q="|cat:$data['order']['latLng']['lat']|cat:","|cat:$data['order']['latLng']['lng']}>Map</a>
                        </p>
                        {/if}</td>
    <td>{($data['order']['pickUpDate']/1000)|date_format} @ {$slotsList[$data['order']['pickUpTime']]['value']}</td>
    <td>{($data['order']['delieveryDate']/1000)|date_format} @ {$slotsList[$data['order']['delieveryTime']]['value']}</td>
    <td> {if isset($data['ordersActivity']['contact'])}{$data['ordersActivity']['contact']['name']} <br> <b>{$data['ordersActivity']['contact']['number']}</b>{else}NA{/if}</td>
    <td> {if $data['ordersActivity']['status'] >= 30 && !isset($data['ordersActivity']['bill'])}
                        <a target="_blank" href="{'create_bill'|path}?ordersActivityId={$data['ordersActivity']['ordersActivityId']}">Create Bill</a>
        {elseif $data['ordersActivity']['status'] >= 30 && isset($data['ordersActivity']['bill'])}

                        Rs {$data['ordersActivity']['bill']['totalBill']}
                        <a target="_blank" href="{'view_bill'|path}?ordersActivityId={$data['ordersActivity']['ordersActivityId']}&displayId={$data['order']['displayId']}"> Bill</a>
        {else}
            NA            
        {/if}
    </td>
    <td>
    {if $data['ordersActivity']['status'] != 50 && $data['ordersActivity']['status'] != 60}
                                            <button type="button" {if $data['ordersActivity']['status'] == 30 && !isset($data['ordersActivity']['bill'])}disabled {/if}onclick="openMoveStageLightBox('{$data['ordersActivity']['ordersActivityId']}','{$data['ordersActivity']['status']}')" class="btn btn-default btn-lg">
                                        
  <span class="glyphicon glyphicon-chevron-right"aria-hidden="true"></span><a href="#myModal" role="button" class="btn" data-toggle="modal">
</button>
{/if}
    </td>
  </tr>
  {/foreach}

</table>
                                <div class="container">
<!--     <div class="row">
        <ul class="pagination">
    <li><a href="#">«</a></li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">»</a></li>
</ul>
    </div> -->
</div>
                        </div>
                    </div>
                </div>
                <div class="content-footer">
                    <p>
                        Page © - 2016 <br>
                        Powered By <a href="http://www.foxbelly.com" target="_blank">Foxbelly</a>
                    </p>
                </div>
            </div>
        </section>
        
    </div>

</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button id="updateContactClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h3>Contact Person</h3>

            </div>
<div class="form-group">
  <label for="usr">Name:</label>
  <input type="text" class="form-control" id="name" name="name" value="">
</div>
<div class="form-group">
  <label for="pwd">Contact Number:</label>
  <input type="text" class="form-control" id="contact" name="contact" value="">
</div>
<input type="button" id="contactUpdate" value="Submit">
        </div>
    </div>
</div>
<form method="post" id="datePickerForm" action="{'scheduled_List'|path}">
  <input type="hidden" id="startDate" name="startDate" value="">
  <input type="hidden" id="endDate" name="endDate" value="">
  <input type="hidden" id="target" name="target" value="">
</form>
<input type="hidden" id="orderActivityId" name="orderActivityId" value="">
<input type="hidden" id="orderStatus" name="orderActivityId" value="">
<style type="text/css">
    /*    --------------------------------------------------
    :: General
    -------------------------------------------------- */
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid blue;
  border-bottom: 16px solid blue;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

body {
    font-family: 'Open Sans', sans-serif;
    color: #353535;
}
.content h1 {
    text-align: center;
}
.content .content-footer p {
    color: #6d6d6d;
    font-size: 12px;
    text-align: center;
}
.content .content-footer p a {
    color: inherit;
    font-weight: bold;
}

/*  --------------------------------------------------
    :: Table Filter
    -------------------------------------------------- */
.panel {
    border: 1px solid #ddd;
    background-color: #fcfcfc;
}
.panel .btn-group {
    margin: 15px 0 30px;
}
.panel .btn-group .btn {
    transition: background-color .3s ease;
}
.table-filter {
    background-color: #fff;
    border-bottom: 1px solid #eee;
}
.table-filter tbody tr:hover {
    cursor: pointer;
    background-color: #eee;
}
.table-filter tbody tr td {
    padding: 10px;
    vertical-align: middle;
    border-top-color: #eee;
}
.table-filter tbody tr.selected td {
    background-color: #eee;
}
.table-filter tr td:first-child {
    width: 38px;
}
.table-filter tr td:nth-child(2) {
    width: 35px;
}
.ckbox {
    position: relative;
}
.ckbox input[type="checkbox"] {
    opacity: 0;
}
.ckbox label {
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.ckbox label:before {
    content: '';
    top: 1px;
    left: 0;
    width: 18px;
    height: 18px;
    display: block;
    position: absolute;
    border-radius: 2px;
    border: 1px solid #bbb;
    background-color: #fff;
}
.ckbox input[type="checkbox"]:checked + label:before {
    border-color: #2BBCDE;
    background-color: #2BBCDE;
}
.ckbox input[type="checkbox"]:checked + label:after {
    top: 3px;
    left: 3.5px;
    content: '\e013';
    color: #fff;
    font-size: 11px;
    font-family: 'Glyphicons Halflings';
    position: absolute;
}
.table-filter .star {
    color: #ccc;
    text-align: center;
    display: block;
}
.table-filter .star.star-checked {
    color: #2F4F4F;
}
.table-filter .star:hover {
    color: #ccc;
}
.table-filter .star.star-checked:hover {
    color: #F0AD4E;
}
.table-filter .media-photo {
    width: 35px;
}
.table-filter .media-body {
    display: block;
    /* Had to use this style to force the div to expand (wasn't necessary with my bootstrap version 3.3.6) */
}
.table-filter .media-meta {
    font-size: 11px;
    color: #999;
}
.table-filter .media .title {
    color: #2BBCDE;
    font-size: 14px;
    font-weight: bold;
    line-height: normal;
    margin: 0;
}
.table-filter .media .title span {
    font-size: .8em;
    margin-right: 20px;
}
.table-filter .media .title span.pagado {
    color: #5cb85c;
}
.table-filter .media .title span.pendiente {
    color: #f0ad4e;
}
.table-filter .media .title span.cancelado {
    color: #d9534f;
}
.table-filter .media .summary {
    font-size: 14px;
}
.active {
  color: green !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {
    $('#'+'{$target}').addClass("active");
    $('.star').on('click', function () {
      $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function () {
      $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function () {
      var $target = $(this).data('target');
      $(this).addClass("active");
         if ($target != 'all') {
        $('.table tr').css('display', 'none');
        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
      } else {
        $('.table tr').css('display', 'none').fadeIn('slow');
      }
      $('#target').val($target);
      $(".table-filter").html('<div style="margin:0 auto; width:230px;text-align:center"><img style="width:30px" src="http://img.naukimg.com/s/3/300/i/loadImage.gif" align="middle" /></div>');
      document.getElementById("datePickerForm").submit();
      document.getElementById("datePickerForm").reset();
   
    });

 });
</script>

<script type="text/javascript">
$(function() {
    // moment().tz('Asia/Kolkata')
    var start = moment().subtract(6, 'days');
    var end = moment();
    $('#startDate').val(start.format('LLL'));
    $('#endDate').val(end.format('LLL'));
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    function cb(start, end) {
        $('#startDate').val(start.format('LLL'));
        $('#endDate').val(end.format('LLL'));
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    //cb(start, end);
    
});
</script>
<script type="text/javascript">

function openMoveStageLightBox(orderActivityId,orderStatus){
    $('#orderActivityId').val(orderActivityId);
    $('#orderStatus').val(orderStatus);
}

$("#contactUpdate").click(function(){
       var dataString = 'name='+$("#name").val()+'&contact='+$("#contact").val()+'&orderActivityId='+$('#orderActivityId').val()+'&orderStatus='+$('#orderStatus').val();
        $.ajax({
    type: "POST",
    url: "{'update_contact'|path}",
    data: dataString,
    success: function(data) {
      //  console.log(JSON.parse(data));
         if(data.status){
             $('#successDiv').show();
         }else{
            $('#failureDiv').show();
         }
        $('#updateContactClose').click();
    }
  });
});
</script>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    word-wrap: break-word;
    max-width: 150px;
    height: 50px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>