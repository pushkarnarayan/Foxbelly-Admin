<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<div class="container">
	<div class="row">
		<form class="form-horizontal"  method="post" id="datePickerForm" action="{'tag_print'|path}">
<fieldset>
<legend>Tag Printing</legend>

<div class="form-group">
  <label class="col-md-4 control-label" for="Current Fleet Size">Serial Number</label>  
  <div class="col-md-5">
  <input id="Current Fleet Size" name="serialNumber" type="text" placeholder="Serial Number" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Annual Forklift Purchases">Vendor Name</label>  
  <div class="col-md-5">
  <input id="Annual Forklift Purchases" name="vendorName" type="text" placeholder="eg. PML" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Annual Forklift Purchases">Name</label>  
  <div class="col-md-5">
  <input id="Annual Forklift Purchases" name="customerName" type="text" placeholder="Customer's" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Decision Timeframe">Type</label>
  <div class="col-md-5">
    <select id="Decision Timeframe" name="type" class="form-control">
      <option value="DC">Dry Clean</option>
      <option value="WI">Wash And Iron</option>
      <option value="WF">Wash And Fold</option>
      <option value="Other">Other</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Message">Count</label>  
  <div class="col-md-5">
  <input id="Message" name="count" type="text" placeholder="Total Count" class="form-control input-md">
  <input type="checkbox" id="includeUg" name="includeUg" onclick="showUgCountDiv()">Include Unger Garments<br>
  </div>
</div>
<div id="undergarmentCount" style="display:none" class="form-group">
  <label class="col-md-4 control-label" for="Message">Ug Count</label>  
  <div class="col-md-5">
  <input id="Message" name="ugCount" type="text" placeholder="Under Garments Count" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Print</button>
  </div>
</div>

</fieldset>
</form>

	</div>
</div>

<script type="text/javascript">
  function showUgCountDiv(){
    if($('#includeUg').prop("checked")){
      $('#undergarmentCount').show();
    }else{
      $('#undergarmentCount').hide();
    }
    
  }
</script>