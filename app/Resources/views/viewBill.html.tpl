<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>Order Id</h2>
  <h3>{$displayId}</h3>           
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Item Name</th>
        <th>Units</th>
        <th>Wieight</th>
        <th>Price</th>
        <th>Description/Comment</th>
      </tr>
    </thead>
    
    <tbody>
    {foreach $billDetail['item'] as $item}
      <tr tabindex="1">
        <td>{$item['name']}</td>
        <td>{$item['unit']}</td>
        <td>{$item['weight']}</td>
        <td>{$item['total']}</td>
        <td>{$item['desc']}</td>
      </tr>
    {/foreach}
    </tbody>
  </table>
  <h4>Total Types:</h4> {$billDetail['totalItem']}</br>
  <h4>Total Bill:</h4> Rs {$billDetail['totalBill']} /- 
</div>

</body>
</html>
