
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<div id="successDiv" class="alert alert-success alert-dismissable" style="display: none;">
 <a class="close" onclick="$('.alert').hide()">×</a> 
  <strong>Success!</strong> Bill Saved.
</div>
<div id="failureDiv" class="alert alert-danger alert-dismissable" style="display: none;">
 <a class="close" onclick="$('.alert').hide()">×</a> 
  <strong>Failure!</strong>Bill Not Saved (Refresh and retry).
</div>
<div class="container">
    <div class="row clearfix">
    	<div class="col-md-12 table-responsive">
    	<form id="formSaveBill" action="{'save_bill'|path}" method="post">
			<table class="table table-bordered table-hover table-sortable" id="tab_logic">
				<thead>
					<tr >
						<th class="text-center">
							Item Name
						</th>
					<!-- 	<th class="text-center">
							Item Quantity
						</th> -->
						<th class="text-center">
							Piece
						</th>
    					

						<th class="text-center">
							Kg
						</th>
						<th class="text-center">
							Total
						</th>
						<th class="text-center">
							Notes
						</th>
        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
						</th>
					</tr>
				</thead>
				<tbody>
    				<tr id='addr0' data-id="0" class="hidden">
						<td data-name="name">
						    <input type="text" name='name' id='name' placeholder='Cloth Type' class="form-control"/>
						</td>
					<!-- 	<td data-name="mail">
						    <input type="text" name='mail0' placeholder='Email' class="form-control"/>
						</td> -->
						
    					<td data-name="unit">
						    <select name="unit" id="unit">
        				        <option value="">Select Unit</option>
        				        {for $i=1 to 20}
    					        <option value="{$i}">{$i}</option>
    					        {/for}
						    </select>
						</td>
						<td data-name="weight">
						    <input type="text" name='weight' id="weight" placeholder='Weight in Kg' class="form-control"/>
						</td>
						<td data-name="total">
						    <input type="text" name='total' id="total" placeholder='Total Price' onchange="calculateTotal()" class="form-control"/>
						</td>
						<td data-name="desc">
						    <textarea name="desc" id="desc" placeholder="Description (Optional)" class="form-control"></textarea>
						</td>
                        <td data-name="del">
                            <button nam"del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                        </td>
					</tr>
				</tbody>
            <input type="hidden" id="totalItem" name="totalItem" value="">
            <input type="hidden" id="totalBill" name="totalBill" value="">
            <input type="hidden" id="ordersActivityId" name="ordersActivityId" value={$ordersActivityId}>
			</table>
			</form>
		</div>
	</div>
	<a id="add_row" class="btn btn-default pull-right">Add Row</a>
</div>
<button id="saveBill" type="button" class="btn btn-success center-block">Save Bill of total 0</button>
<style type="text/css">
	.table-sortable tbody tr {
    cursor: move;
}

</style>
<script type="text/javascript">
	$(document).ready(function() {
    $("#add_row").on("click", function() {
        // Dynamic Rows Code
        
        // Get max row id and set new id

        var newid = 0;
        $.each($("#tab_logic tr"), function() {
            if (parseInt($(this).data("id")) > newid) {
                newid = parseInt($(this).data("id"));
            }
        });
        newid++;
        var tr = $("<tr></tr>", {
            id: "addr"+newid,
            "data-id": newid
        });
        
        // loop through each td and create new elements with name of newid
        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
            var cur_td = $(this);
            
            var children = cur_td.children();
            
            // add new td and element if it has a nane
            if ($(this).data("name") != undefined) {
                var td = $("<td></td>", {
                    "data-name": $(cur_td).data("name")
                });
                
                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                c.attr("name", $(cur_td).data("name") + newid);
                c.attr("id", $(cur_td).data("name") + newid);
                c.appendTo($(td));
                td.appendTo($(tr));
            } else {
                var td = $("<td></td>", {
                    'text': $('#tab_logic tr').length
                }).appendTo($(tr));
            }
        });
        
        // add delete button and td
        /*
        $("<td></td>").append(
            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                .click(function() {
                    $(this).closest("tr").remove();
                })
        ).appendTo($(tr));
        */
        
        // add the new row
        $(tr).appendTo($('#tab_logic'));
        
        $(tr).find("td button.row-remove").on("click", function() {
             $(this).closest("tr").remove();
        });
        $('#totalItem').val(newid);
      
});




    // Sortable Code
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
    
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        
        return $helper;
    };
  
    $(".table-sortable tbody").sortable({
        helper: fixHelperModified      
    }).disableSelection();

    $(".table-sortable thead").disableSelection();



    $("#add_row").trigger("click");
});

function calculateTotal(){
	  var total = 0;
	  var newid = 0;
        $.each($("#tab_logic tr"), function() {
            if (parseInt($(this).data("id")) > newid) {
                newid = parseInt($(this).data("id"));
            }
        });
        for(var i=1 ;i <= newid ; i++){
        	total = total + parseInt($('#total'+i).val());
        };
        $('#saveBill').html('Save Bill of Total '+total+' /-');
        $('#totalBill').val(total);
}

</script>
<script type="text/javascript">

	$("#saveBill").click(function(){
		var form = $('#formSaveBill');
		$.ajax( {
      type: "post",
      url: form.attr( 'action' ),
      data: form.serialize(),
      success: function(data) {
        if(data.status){
             $('#successDiv').show();
         }else{
            $('#failureDiv').show();
         }
      }
    } );
  } );
		// console.log("Here");
		// $("#formSaveBill").submit();
// });
</script>>