<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Models\OrdersActivity;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\DiExtraBundle\Annotation as DI;


     
class TagController extends Controller
{

	/**
     * @Route("/tag", name="tag_list")
     */

	public function tagForm(){

		return $this->render('tagList.html.tpl');	
	}

	/**
     * @Route("/print", name="tag_print")
     */
	public function generate(Request $request){
		$postParams = $request->request->all();
		return $this->render('tagPrint.html.tpl',$postParams);	
	}
	
}