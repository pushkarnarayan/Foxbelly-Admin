<?php
namespace AppBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Models\OrdersActivity;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrderListController extends Controller
{
    private $firebase;

    /** @DI\Inject("sendMessage.manager") */
    public $sendMessageManager;

	/**
     * @Route("/orders", name="scheduled_List")
     */
    public function scheduleList(Request $request)
    {	
    	date_default_timezone_set('UTC');
    	$postParams = $request->request->all();
    	$startDate = isset($postParams['startDate']) ? strtotime($postParams['startDate'])*1000 : strtotime(date('Y-m-d h:i:s', strtotime('-6 days')))*1000;
    	$endDate = isset($postParams['endDate']) ? strtotime($postParams['endDate'])*1000 : strtotime(date('Y-m-d h:i:s', strtotime('+1 days')))*1000;
    	if(!isset($postParams['target'])){
    		$status = OrdersActivity::getStatusCodeForButtonClick('scheduled');
            $target = 'scheduled';
    	}else{
    		$status = OrdersActivity::getStatusCodeForButtonClick($postParams['target']);
            $target = $postParams['target'];
    	}
    	$totalOrdersData = [];
    	$this->firebase = new \Firebase\FirebaseLib('https://foxbellywash.firebaseio.com/', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjg2NTQ5NDE4Nzc1MSwidiI6MCwiZCI6eyJ1aWQiOiJrYXRvIn0sImlhdCI6MTQ5NDI3NDE1MX0.3B7R5AHB7P0L0JFSQ_hwu7OIIm0WWY3uUhY5LUC3LRk');
        $totalOrdersActivityJson = $this->firebase->get('/ordersActivity',array('orderBy' => '"updatedOn"','startAt'=>$startDate,'endAt'=>$endDate));
        $totalOrdersActivityList = json_decode($totalOrdersActivityJson,true);
        foreach($totalOrdersActivityList as $ordersActivityId=>$ordersActivity){
            $ordersActivity['ordersActivityId'] = $ordersActivityId;
 			if($ordersActivity['status'] == $status){
 			// $user = $this->firebase->get('/users/'.$ordersActivity['userId']);
    //     	$ordersData['user'] = json_decode($user,true);
        	$order = $this->firebase->get('/orders/'.$ordersActivity['orderId']);
        	$ordersData['order'] = json_decode($order,true);
        	$ordersData['ordersActivity'] = $ordersActivity;
        	$totalOrdersData[] = $ordersData;
 			}
        	
        }
        $statusDisplayText = OrdersActivity::getAllStatusDisplayText();
        $slotsJson = $this->firebase->get('slots');
        $slotsList = json_decode($slotsJson,true);
        $param =  $this->render('orders.html.tpl',array('totalOrdersData'=>$totalOrdersData,'statusDisplayText'=>$statusDisplayText,'slotsList'=>$slotsList,'target'=>$target));
        return $param;
    }


    /**
     * @Route("/updateContact", name="update_contact")
     */
    public function updateContact(Request $request)
    {
        $postParams = $request->request->all();
        $this->firebase = new \Firebase\FirebaseLib('https://foxbellywash.firebaseio.com/', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjg2NTQ5NDE4Nzc1MSwidiI6MCwiZCI6eyJ1aWQiOiJrYXRvIn0sImlhdCI6MTQ5NDI3NDE1MX0.3B7R5AHB7P0L0JFSQ_hwu7OIIm0WWY3uUhY5LUC3LRk');
        $contact = array('contact'=>array('name'=>$postParams['name'],'number'=>$postParams['contact']),'status'=>OrdersActivity::getNextStatus($postParams['orderStatus']));
        $this->firebase->update('/ordersActivity/'.$postParams['orderActivityId'],$contact);
       // $this->sendTextMessage();
        return new JsonResponse(array('status'=>true));
    }

    public function sendTextMessage(){
        $mobileNumber = '9862523332';
        $senderId = 'FOXBLY';
        $message = 'Hello';
        $this->sendMessageManager->sendTextMessage($mobileNumber,$senderId,$message);
    }

}