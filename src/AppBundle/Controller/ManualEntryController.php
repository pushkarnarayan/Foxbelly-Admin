<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Models\OrdersActivity;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\DiExtraBundle\Annotation as DI;


     
class ManualEntryController extends Controller
{

	private $firebase;
	

	public function __construct(){
		$this->firebase = new \Firebase\FirebaseLib('https://foxbellywash.firebaseio.com/', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjg2NTQ5NDE4Nzc1MSwidiI6MCwiZCI6eyJ1aWQiOiJrYXRvIn0sImlhdCI6MTQ5NDI3NDE1MX0.3B7R5AHB7P0L0JFSQ_hwu7OIIm0WWY3uUhY5LUC3LRk');
	}

	/**
     * @Route("/entry/{success}", name="manual_entry")
     */
	public function manualEntryForm($success = 0){
		return $this->render('manualEntryForm.html.tpl',array('success'=>$success));	
	}

	/**
     * @Route("/saveEntry", name="manual_entry_save")
     */
	public function generate(Request $request){
		$postParams = $request->request->all();
		$this->placeOrder($postParams);
		return $this->redirect($this->generateUrl('manual_entry',array('success'=>true)));

	}
	
	public function placeOrder($postParams){
		date_default_timezone_set('UTC');
		$orderObject = [
        "displayId" => $postParams['serialNumber'],
        "location" => $postParams['address'],
        "name" => $postParams['name'],
        "contact" => $postParams['contact'],
        "pickUpDate" => strtotime(date($postParams['pickUpDate']))*1000,
        "pickUpTime" => $postParams['pickUpTime'],
        "delieveryDate" => strtotime(date($postParams['delieveryDate']))*1000,
        "delieveryTime" =>$postParams['delieveryTime'],
        "addedOn" => strtotime(date('Y-m-d H:i'))*1000,
        "latLng" => null
      ];
      $res = $this->firebase->push('/orders',$orderObject);
      $resArray = json_decode($res,true);
      $activityObject = [
        "orderId" => $resArray['name'],
        "userId" => null,
        "status" => 10,
        "addedOn" => strtotime(date('Y-m-d H:i'))*1000,
        "updatedOn" => strtotime(date('Y-m-d H:i'))*1000,
        "contact" => null,
        "activityBy" => 'admin',
        "remark" => null
      ];
      $this->firebase->push('/ordersActivity',$activityObject);
	}
	/**
     * @Route("/getSerial", name="get_serial_number")
     */
	public function getSerialNumber(){
        $orderId = $this->firebase->get('/orderId');
        $displayIOrderId = $this->generateRandomId($orderId);
        return new JsonResponse($displayIOrderId);
	}

	public function generateRandomId($orderId) {
      $orderIdMaxLength =  8;
      $paddingText = $this->getRandomText($orderIdMaxLength - strlen((string)$orderId));
      $displayOrderId = $paddingText . $orderId;
      return $displayOrderId;
  }

  public function getRandomText($length) {
    $result = "";
    $possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for ($i = 0; $i < $length; $i++){
        $result .= $possible[mt_rand(0, 25)];
    }
    return $result;
  }
}