<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Models\OrdersActivity;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/bill")
 */

class BillController extends Controller
{
    
	/**
     * @Route("/create", name="create_bill")
     */
    public function createBill(Request $request){
    	$ordersActivityId = $request->query->get('ordersActivityId');
    	$param =  $this->render('createBill.html.tpl',array('ordersActivityId'=>$ordersActivityId));
    	return $param;
    }

    /**
     * @Route("/save", name="save_bill")
     */
    public function saveBill(Request $request)
    {	$postParams = $request->request->all();
    	file_put_contents('/tmp/postParams',var_export($postParams,true));
    	$bill = [];
    	$totalBillCalculated = 0;
    	for($i=1;$i<= $postParams['totalItem'];$i++){
    		$bill['item']['item'.$i] =  ['name'=>$postParams['name'.$i],'unit'=>$postParams['unit'.$i],'weight'=>$postParams['weight'.$i],'total'=>$postParams['total'.$i],'desc'=>$postParams['desc'.$i]];
    		$totalBillCalculated = $totalBillCalculated + $postParams['total'.$i];
    	}
    	$bill['totalItem'] = $postParams['totalItem'];
    	$bill['totalBill'] = $postParams['totalBill'];
    	if($totalBillCalculated == $bill['totalBill']){
    		$firebase = new \Firebase\FirebaseLib('https://foxbellywash.firebaseio.com/', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjg2NTQ5NDE4Nzc1MSwidiI6MCwiZCI6eyJ1aWQiOiJrYXRvIn0sImlhdCI6MTQ5NDI3NDE1MX0.3B7R5AHB7P0L0JFSQ_hwu7OIIm0WWY3uUhY5LUC3LRk');
    		$firebase->update('/ordersActivity/'.$postParams['ordersActivityId'],array('bill'=>$bill));
    		return new JsonResponse(array('status'=>true));
    	}else
    		return new JsonResponse(array('status'=>false));
    	}
    	
    /**
     * @Route("/view", name="view_bill")
     */
    public function ViewBill(Request $request)
    {	
    	$ordersActivityId = $request->query->get('ordersActivityId');
    	$displayId = $request->query->get('displayId');
    	$firebase = new \Firebase\FirebaseLib('https://foxbellywash.firebaseio.com/', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjg2NTQ5NDE4Nzc1MSwidiI6MCwiZCI6eyJ1aWQiOiJrYXRvIn0sImlhdCI6MTQ5NDI3NDE1MX0.3B7R5AHB7P0L0JFSQ_hwu7OIIm0WWY3uUhY5LUC3LRk');
    	$billDetail = $firebase->get('/ordersActivity/'.$ordersActivityId.'/bill');
    	file_put_contents('/tmp/billDetail',var_export($billDetail,true));
    	return $this->render('viewBill.html.tpl',array('ordersActivityId'=>$ordersActivityId,'billDetail'=>json_decode($billDetail,true),'displayId'=>$displayId));
    }

    
}