<?php
namespace AppBundle\Models;

class OrdersActivity{
  const STATUS_SCHEDULED = 10;
  const STATUS_READY_FOR_PICK_UP = 20;
  const STATUS_IN_PROCESS = 30;
  const STATUS_OUT_FOR_DELIEVERY = 40;
  const STATUS_DELIEVERED = 50;
  const STATUS_CANCELLED = 60;

  const orderId_List  = array(self::STATUS_SCHEDULED,self::STATUS_READY_FOR_PICK_UP,self::STATUS_IN_PROCESS,self::STATUS_OUT_FOR_DELIEVERY,self::STATUS_DELIEVERED,self::STATUS_CANCELLED);

  public static function getNextStatus($statusId){
  	$nextToReturn = false;
  	foreach(self::orderId_List as $id){
  		if($nextToReturn){
  			return $id;
  		}
  		if($id == $statusId){
  			$nextToReturn = true;
  		}
  		

  	}
  }
   

  public static function getAllStatusDisplayText() {
    $statusArray = [];
    $statusArray[self::STATUS_SCHEDULED] = 'Scheduled';
    $statusArray[self::STATUS_READY_FOR_PICK_UP] = 'Ready For Pick-Up';
    $statusArray[self::STATUS_IN_PROCESS] = 'In Process';
    $statusArray[self::STATUS_OUT_FOR_DELIEVERY] = 'Out For Delievery';
    $statusArray[self::STATUS_DELIEVERED] = 'Delievered';
    $statusArray[self::STATUS_CANCELLED] = 'Cancelled';
    return $statusArray;
  }

  public static function getStatusCodeForButtonClick($buttonId){
  	$statusArray = [];
    $statusArray['scheduled'] = self::STATUS_SCHEDULED;
    $statusArray['pickup'] = self::STATUS_READY_FOR_PICK_UP;
    $statusArray['inprocess'] = self::STATUS_IN_PROCESS;
    $statusArray['outfordelievery'] = self::STATUS_OUT_FOR_DELIEVERY;
    $statusArray['delievered'] = self::STATUS_DELIEVERED;
    $statusArray['cancelled'] = self::STATUS_CANCELLED;
    return $statusArray[$buttonId];
  }

}